import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from '../models/usuario';
import { AccountService } from '../services/account.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  public token;
  public sessionId;
  public usuarioServer;
  public usuario: Usuario;
  public haySesion = false;
  
  constructor(private _accountService: AccountService, private _router: Router) {
    this.usuario = new Usuario("","","");
    
  }

  ngOnInit(): void { 
    //Antes de todo comprobamos si hay sesion
    if(sessionStorage.getItem('session_id')){
      //Si hay sesion redirigimos al perfil
      this._router.navigate(['/perfil']);
    } else { //Si no hay sesion, a que se loguee
      this._router.navigate(['/login']);
    }
  }

//Funcion que realiza el inicio de sesion (Obtener token -> Validarlo -> Solicitar Id de sesion)
  logIn() {
    this._accountService.getToken().subscribe(
      response => {
        //Obtenemos el token
         sessionStorage.setItem('token', response.request_token);
         console.log("TOKEN:  " + response.request_token);

         //Recogemos los datos del usuario desde el formulario
         let username = (<HTMLInputElement>document.getElementById("usuario")).value;
         let password = (<HTMLInputElement>document.getElementById("password")).value;

         //Validamos el token
         this._accountService.validateLogIn(username, password, response.request_token).subscribe(
          response => {
              console.log("TOKEN VALIDADO:  " + response.request_token);
    
              //Obtenemos ID Sesion
              this._accountService.getSessionId(response.request_token).subscribe(
                response => {
                  console.log("SESSION_ID:  " + response.session_id);
                  sessionStorage.setItem('session_id', response.session_id);
                  if(sessionStorage.getItem('session_id') != " "){
                    this.haySesion = true;
                    sessionStorage.setItem("sesion", this.haySesion.toString());

                    //Una vez que tenemos el usuario logueado, obtenemos sus datos
                    this.getDatosUsuario(sessionStorage.getItem('session_id'));

                    //Lo redirigimos a su perfil
                    this._router.navigate(['/perfil']);
                  }
                },
                error => {
                  console.log(error);
                }
              );
          }
        );
      },
      error =>{
        console.log(error);
      }
    );
   
  }

  //Metodo para obtener los datos del usuario
  getDatosUsuario(sessionId){
    this._accountService.getDatosUsuario(sessionId).subscribe(
      response => {
        this.usuario = new Usuario(response.id, response.username, response.iso_3166_1);
        sessionStorage.setItem('datosUsuario', JSON.stringify(this.usuario));
        console.log(response.username);
      },
      error => {
        console.log(error);
      }
    );
  }

  logOut(){
    sessionStorage.clear();
    localStorage.clear();
  }


  
}
