import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GLOBAL } from '../services/global';
import { Pelicula} from '../models/Pelicula';
import { PeliculasService } from '../services/peliculas.service';

@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.css']
})
export class PeliculaComponent implements OnInit {

  public pelicula: Pelicula;
  public status;
  public haySesion;
  //Para obtener las favoritas
  public arrayFavoritas;
  public listaIdFavoritas = [];;

  constructor(private _peliculasService: PeliculasService,  private _router: Router, private _route: ActivatedRoute) {
      this.pelicula = new Pelicula("","","","","","","");
        //Comprobamos si hay sesion
        this.haySesion = sessionStorage.getItem('sesion');
   }

  ngOnInit(): void {
    //Metodo para cargar la pagina
    this.loadPage();
  }

  ngDoCheck() {
    if(this.haySesion == "true"){
      this.getFavoritas();
    }
     
  }


  //Metodo para obtener el id de la pelicula desde la URL, y obtener la pelicula con ese ID
  loadPage(){
    this._route.params.subscribe(params => {
      let id = params['id'];
      this.getPelicula(id);
    });
  }

  //Metodo para obtener una pelicula segun su ID
  getPelicula(idPelicula){
    this._peliculasService.getPelicula(idPelicula).subscribe(
     
      response =>{
          if(response){ //Tenemos respuesta

              //Rellenamos los datos de la pelicula
              this.pelicula._id = response.id;
              this.pelicula.nombre = response.original_title;
              this.pelicula.descripcion = response.overview;
              this.pelicula.anyo = response.release_date.substring(0,4);
              
              //Para rellenar el director, llamamos a la funcion que lo obtiene
              this.getDirectorPelicula(idPelicula);

              //Para rellenar la foto de la pelicula, concatenamos la direccion web que nos las sirve, concatenando el nombre de la foto
              this.pelicula.foto = "https://image.tmdb.org/t/p/w500/" + response.poster_path;

              this.pelicula.puntuacion = response.vote_average;

              console.log(response);
          } else { //Error
            this.status = 'error';
          }
      },
      error =>{ // Si hay un error de escritura en URL redirigimos a error
        //this._router.navigate(['/error']);
        console.log("Error en la peticion");
      }
    );
  }


  //Metodo para obtener el director de una pelicula segun su ID
  getDirectorPelicula(idPelicula){
    this._peliculasService.getDirectorPelicula(idPelicula).subscribe(
     
      response =>{
          if(response){ //Tenemos respuesta
              //Obtenemos al director (Realizamos un ciclo for sobre todo el json, filtrando por el puesto de los empleados, buscando al director)
              for(let i = 0; i < response.crew.length; i++){
                if(response.crew[i].job == "Director"){
                  this.pelicula.director = response.crew[i].name;
                }
              }
              console.log(response);
             
          } else { //Error
            this.status = 'error';
          }
      },
      error =>{ // Si hay un error de escritura en URL redirigimos a error
        //this._router.navigate(['/error']);
        console.log("Error en la peticion");
      }
    );
  }

  //Metodo para agregar una pelicula a favoritas
  addFavoritas(idPelicula) {
    this._peliculasService.addFavoritas(idPelicula).subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.log(error);
      }
    );
  }

  //Obtener mis peliculas favoritas y meterlas en una lista
  getFavoritas() {
    this._peliculasService.getFavoritas().subscribe(
      response => {
       this.arrayFavoritas = response.results;
       this.arrayFavoritas.forEach(pelicula => {
         this.listaIdFavoritas.push(pelicula.id);
       });
      },
      error => {
        console.log(error);
      }
    );
  }

}
