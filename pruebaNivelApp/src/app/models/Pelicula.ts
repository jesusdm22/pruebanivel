export class Pelicula {

    constructor(
        public _id: string, 
        public nombre: string, 
        public descripcion: string, 
        public anyo: string,
        public foto: string,
        public puntuacion: string,
        public director?: string
    ){}
}