import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { LoginComponent } from './login/login.component';
import { PeliculaComponent } from './pelicula/pelicula.component';
import { PerfilComponent } from './perfil/perfil.component';
import { TopPelisComponent } from './top-pelis/top-pelis.component';

const routes: Routes = [
  //Rutas a los componentes
  {path: '', redirectTo: 'inicio', pathMatch: 'full'},
  {path: 'inicio', component: InicioComponent},
  {path: 'pelicula/:id', component: PeliculaComponent},
  {path: 'login', component: LoginComponent},
  {path: 'top-pelis', component: TopPelisComponent},
  {path: 'perfil', component: PerfilComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
