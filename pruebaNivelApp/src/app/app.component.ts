import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'pruebaNivelApp';
  haySesion;
  nombreUsuario;

  constructor(private _router: Router, private _route: ActivatedRoute) {
  
 }
  ngOnInit():void {
    this.haySesion = sessionStorage.getItem('sesion');
    if(this.haySesion){
      this.nombreUsuario = JSON.parse(sessionStorage.getItem('datosUsuario')).nombre;
    }
  } 

  ngDoCheck(){
  
    //Se chequeara si hay sesion o no, para mostrar un texto u otro en la barra de navegacion

    //Obtenemos la sesion
    let comprobacion = sessionStorage.getItem('sesion');

    //Si hay sesion, se lo decimos al front, y obtenemos el username
    if(comprobacion == "true") {
      this.haySesion = true;
      this.nombreUsuario = JSON.parse(sessionStorage.getItem('datosUsuario')).nombre;
    } else { 
      //Si no, que deje el texto por defecto
      this.haySesion = false;
    }
    
  }

  logOut(){
    sessionStorage.clear();
    localStorage.clear();
    this.haySesion = false;
    this._router.navigate(['/login']);
  }
}
