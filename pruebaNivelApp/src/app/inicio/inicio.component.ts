import { Component, OnInit } from '@angular/core';
import { PeliculasService } from '../services/peliculas.service';
import { Router } from '@angular/router';
import { GLOBAL } from '../services/global';
import { Pelicula} from '../models/Pelicula';
import { strict } from 'assert';
import { stringify } from 'querystring';
import { last } from 'rxjs-compat/operator/last';
import { reduce } from 'rxjs-compat/operator/reduce';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  public pelicula: Pelicula;
  public peliculas;
  public listaPeliculas : Pelicula [] ;
  public status;
  public haySesion;

  //Auxiliares
  public _id;
  public nombre;
  public descripcion;
  public url_foto = GLOBAL.url_foto;
  public foto;
  public puntuacion;
  public director;
  public anyo;
  public paginas;
  public paginaActual;

  //Para obtener las favoritas
  public arrayFavoritas;
  public listaIdFavoritas = [];;
  

  constructor(private _peliculasService: PeliculasService,  private _router: Router) {

      //Comprobamos si hay sesion
      this.haySesion = sessionStorage.getItem('sesion');

      //Inicializamos una pelicula vacia
      this.pelicula = new Pelicula("","","","","","","");
      this.listaPeliculas = Array();
      //Creamos el array de paginas
      this.paginas = [1, 2, 3, 4, 5];

       //Cargamos la primera pagina
      this.getMasVotadas(1);
      //La almacenamos en sesion
      sessionStorage.setItem('lastClicked', '1');
      //Establecemos la pagina actual en una variable para poder usarla mas comodamente
      this.paginaActual = sessionStorage.getItem('lastClicked');

      //Si hay sesion, cargamos las favoritas del usuario
      if(this.haySesion == "true") {
        this.getFavoritas();
        
      }
   }

  ngOnInit(): void {
   
  }

  ngDoCheck() {
    if(this.haySesion == "true"){
      this.getFavoritas();
    }
     
  }


  //Metodo para recuperar las peliculas de una pagina
  getPaginaSiguiente(nPagina) {
    //Obtenemos las peliculas de esa pagina
    this.getMasVotadas(nPagina);
    //Animacion para volver al inicio cuando clickemos en una pagina
    document.body.scrollIntoView({behavior: 'smooth', block: 'start'});

    //Condicion para dar un estilo al boton de la pagina a actual
    if (document.getElementById(nPagina).innerText == nPagina){
      document.getElementById(nPagina).style.backgroundColor = 'black';
      document.getElementById(nPagina).style.color = 'white';
    }

    //Condicion para devolver el estilo a la ultima pagina clicada
    if(document.getElementById(sessionStorage.getItem('lastClicked')) != nPagina) {
      document.getElementById(sessionStorage.getItem('lastClicked')).style.backgroundColor = 'white';
      document.getElementById(sessionStorage.getItem('lastClicked')).style.color = 'blue';
    }

    //Almacenamos en sesion la ultima pagina clicada
    sessionStorage.setItem('lastClicked', nPagina);

    //Guardamos la pagina actual para llamarla en HTML
    this.paginaActual = sessionStorage.getItem('lastClicked');
  }


  getMasVotadas(nPagina) {
    this._peliculasService.getMasVotadas(nPagina).subscribe(

      response => {
        this.peliculas = response;
        this.peliculas = this.peliculas.results;
      
      },
      error =>{ // Si hay un error de escritura en URL redirigimos a error
        //this._router.navigate(['/error']);
        console.log("Error en la peticion");
      }
    );

    return this.peliculas;
  }



  //Metodo para obtener una pelicula segun su ID
  getPelicula(idPelicula){
    this._peliculasService.getPelicula(idPelicula).subscribe(
     
      response =>{
          if(response){ //Tenemos respuesta

              //Rellenamos los datos de la pelicula
              this.pelicula._id = response.id;
              this.pelicula.nombre = response.original_title;
              this.pelicula.descripcion = response.overview;
              this.pelicula.anyo = response.release_date.substring(0,4);
        
              //Para rellenar el director, llamamos a la funcion que lo obtiene
              this.getDirectorPelicula(idPelicula);

              //Para rellenar la foto de la pelicula, concatenamos la direccion web que nos las sirve, concatenando el nombre de la foto
              this.pelicula.foto = "https://image.tmdb.org/t/p/w500/" + response.poster_path;

              this.pelicula.puntuacion = response.vote_average;

              console.log(response);
          } else { //Error
            this.status = 'error';
          }
      },
      error =>{ // Si hay un error de escritura en URL redirigimos a error
        //this._router.navigate(['/error']);
        console.log("Error en la peticion");
      }
    );
  }



  //Metodo para obtener una pelicula segun su ID : DEVUELVE OBJETO PELICULA
   getPeliculaObjeto(idPelicula) : Pelicula {
     let p;
    this._peliculasService.getPelicula(idPelicula).subscribe(
    
      response =>{
          if(response){ //Tenemos respuesta

              //Rellenamos los datos de la pelicula
              this._id= response.id;
              this.nombre = response.original_title;
              this.descripcion = response.overview;
              this.anyo = response.release_date.substring(0,4);
              this.director = this.getDirectorPelicula(idPelicula);
              this.foto = this.pelicula.foto = "https://image.tmdb.org/t/p/w500/" + response.poster_path;
              this.puntuacion = response.vote_average;
              let p = new Pelicula(this._id, this.nombre, this.descripcion, this.anyo, this.director, this.foto, this.puntuacion);

          } else { //Error
            this.status = 'error';
          }

          
      },
      error =>{ // Si hay un error de escritura en URL redirigimos a error
        //this._router.navigate(['/error']);
        console.log("Error en la peticion");
      }
    );
    //console.log(new Pelicula(this._id, this.nombre, this.descripcion, this.anyo, this.director, this.foto, this.puntuacion));
    return p;
  }


  //Metodo para obtener el director de una pelicula segun su ID
  getDirectorPelicula(idPelicula){
    this._peliculasService.getDirectorPelicula(idPelicula).subscribe(
     
      response =>{
          if(response){ //Tenemos respuesta
              //Obtenemos al director (Realizamos un ciclo for sobre todo el json, filtrando por el puesto de los empleados, buscando al director)
              for(let i = 0; i < response.crew.length; i++){
                if(response.crew[i].job == "Director"){
                  this.pelicula.director = response.crew[i].name;
                }
              }
              console.log(response);
             
          } else { //Error
            this.status = 'error';
          }
      },
      error =>{ // Si hay un error de escritura en URL redirigimos a error
        //this._router.navigate(['/error']);
        console.log("Error en la peticion");
      }
    );
  }

  //Metodo para agregar una pelicula a favoritas
  addFavoritas(idPelicula) {
    this._peliculasService.addFavoritas(idPelicula).subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.log(error);
      }
    );
  }

  //Obtener mis peliculas favoritas y meterlas en una lista
  getFavoritas() {
    this._peliculasService.getFavoritas().subscribe(
      response => {
       this.arrayFavoritas = response.results;
       this.arrayFavoritas.forEach(pelicula => {
         this.listaIdFavoritas.push(pelicula.id);
       });
      },
      error => {
        console.log(error);
      }
    );
  }

}

