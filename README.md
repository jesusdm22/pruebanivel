# **Este proyecto es una prueba de nivel realizada por Jesús Díaz Muñoz como parte del proceso de selección para un puesto de trabajo.**  

-------------------------------------------------------------------
## Descripción de la aplicación.  
-------------------------------------------------------------------  
El proyecto consiste en una aplicación para listar peliculas de forma paginada y agregar éstas a favoritos.    
El usuario tendrá loguearse con su cuenta de [TheMovieDB](https://www.themoviedb.org/login).  
Las películas se encuentran listadas en la pestaña **"Inicio"** de la barra superior de navegación.  
Posteriormente, el usuario podrá verlas en su perfil, haciendo click en la parte superior derecha de la barra de navegación donde aparece su nombre de usuario.  

**Para usar la aplicación es obligatorio tener una cuenta creada la página mencionada anteriormente.**. ([TheMovieDB](https://www.themoviedb.org/login))
**De todas formas, les adjunto un usuario ya creado, el cual pueden usar para realizar las pruebas pertinentes si no quieren crear un perfil**  
  
usuario: **usuarioPrueba**  
password: **1234**

-------------------------------------------------------------------
## Requisitos para lanzar el proyecto.
-------------------------------------------------------------------
- Angular 
- Node
- NPM
- Docker (Opcional)

-------------------------------------------------------------------
## Pasos para lanzar la aplicacion en modo desarrollo con Angular.
-------------------------------------------------------------------
1. Entrar a la carpeta del proyecto **"pruebaNivelApp"**
2. Ejecutar el comando:
	**ng serve**

3. Entrar al navegador con la URL:
	**localhost:4200 (puerto por defecto de Angular)**  
	
7. El proyecto estara funcionando.

-------------------------------------------------------------------
## Pasos para lanzar app con Docker.
-------------------------------------------------------------------
1. Entrar a la carpeta del proyecto "pruebaNivelApp"
2. Construir la imagen docker del proyecto con el comando:
	 **docker build -t pruebanivelapp .**

3. Comprobar que se haya creado la imagen:
	**docker images**

4. Lanzar la imagen con el comando:
	**docker run -d -it -p 80:80 pruebanivelapp**

5. Comprobar que se haya lanzado la app correctamente:
	**docker ps -a**

6. Entrar a un navegador y escribir en la URL:
	**localhost**

7. El proyecto estara funcionando.