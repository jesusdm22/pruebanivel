import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { GLOBAL } from '../services/global';

@Injectable({
  providedIn: 'root',
})
export class AccountService {
  public api: string;
  public api_key: string;

  constructor(public _http: HttpClient) {
    this.api = GLOBAL.api; //Llamamos al fichero con datos globales (URL de la API)
    this.api_key = GLOBAL.api_key; //Llamamos al fichero con datos globales (API_KEY)
  }

  //METODOS ----------------------------------------------------------------------------------------------------------------------

  //Metodo para obtener un token valido
  getToken() : Observable <any> {
    return this._http.get(
      this.api + '/authentication/token/new?api_key=' + this.api_key
    );
  }


  //Metodo para loguear usuario y validar token
  validateLogIn(user, pass, token) : Observable <any> {
    
    let params = {username: user, password: pass, request_token: token};

    let headers = new HttpHeaders().set('Content-Type', 'application/json')
                                   .set('Authorization', token);

    return this._http.post(this.api+'authentication/token/validate_with_login?api_key=' + this.api_key, params, {headers:headers});
  }

  //Metodo para solicitar un session_id
  getSessionId(token) : Observable <any> {

    let params = {request_token: token};
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
                                  

    return this._http.post(this.api + 'authentication/session/new?api_key=' + this.api_key, params, {headers: headers}
  
    );
  }


  //Metodo para obtener los datos de un usuario
  getDatosUsuario(sessionId)  : Observable <any>{
    return this._http.get(this.api + 'account?api_key=' + this.api_key + '&session_id=' + sessionId);
  }

}
