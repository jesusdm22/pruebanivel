import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {GLOBAL} from '../services/global';
import {Pelicula} from '../models/Pelicula';


@Injectable({
  providedIn: 'root'
})
export class PeliculasService {

  public api: string;
  public api_key: string;

  constructor(public _http: HttpClient) { 
    this.api = GLOBAL.api;  //Llamamos al fichero con datos globales (URL de la API)
    this.api_key = GLOBAL.api_key; //Llamamos al fichero con datos globales (API_KEY)

  }


  //METODOS ----------------------------------------------------------------------------------------------------------------------

  //Metodo para pedir peticion HTTP al API y obtener la pelicula por ID
  getPelicula(idPelicula): Observable<any> {
    return this._http.get(this.api + 'movie/'+ idPelicula + '?api_key=' + this.api_key);
  }


  //Metodo para pedir peticion HTTP al API y obtener los datos de los trabajadores de la pelicula
  getDirectorPelicula(idPelicula): Observable<any> {
    return this._http.get(this.api + 'movie/'+ idPelicula + '/credits' + '?api_key=' + this.api_key);
  }


  //Metodo para obtener las peliculas mas votadas
  getMasVotadas(nPagina) : Observable<any>{
    return this._http.get(this.api + 'movie/top_rated?api_key=' + this.api_key + '&page=' + nPagina);
   
  }

  //Metodo para obtener las peliculas favoritas de un usuario (usando un session_id)
  getFavoritas() : Observable<any>{
    let session_id = sessionStorage.getItem('session_id');
    return this._http.get(this.api +'account/{account_id}/favorite/movies?api_key=' + this.api_key + '&session_id=' +  session_id + '&sort_by=created_at.asc');
  }

  //Metodo para agregar una pelicula a favoritos
  addFavoritas(idPelicula) : Observable<any>{
    //Establecemos los parametros para el POST
    let params = {media_type: "movie", media_id: idPelicula, favorite: true};
    let headers = new HttpHeaders().set('Content-Type', 'application/json');
    //Obtenemos el id e sesion
    let session_id = sessionStorage.getItem('session_id');     

    return this._http.post(this.api + 'account/{account_id}/favorite?api_key=' + this.api_key + '&session_id=' + session_id, params, {headers: headers});
  }
}
