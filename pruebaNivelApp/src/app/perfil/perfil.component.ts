import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pelicula } from '../models/Pelicula';
import { Usuario } from '../models/usuario';
import { AccountService } from '../services/account.service';
import { PeliculasService } from '../services/peliculas.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  public usuario:Usuario;
  public listaPeliculas = [];
  public peliculas = <any>[];
  public cargada:boolean;
  public message: string;

  constructor(private _accountService: AccountService, private _peliculasService: PeliculasService ,private _router: Router) { 
    this.usuario = new Usuario("","","");
    this.cargada = false;
    this.message = "Cargando...";
  }
  
  ngOnInit(): void {
    //Antes de todo comprobamos si hay sesion
    if(sessionStorage.getItem('session_id')){
      //Si hay sesion redirigimos al perfil
      this._router.navigate(['/perfil']);

      //Nos traemos sus datos
      this.getDatos();

      //Obtenemos sus peliculas favoritas
      this.getFavoritas();

    } else { //Si no hay sesion, a que se loguee
      this._router.navigate(['/login']);
    }
  }

  //Metodo para obtener los datos del usuario
  getDatos(){ 
    this._accountService.getDatosUsuario(sessionStorage.getItem('session_id')).subscribe(
      response => {
          //Construimos un usuario con la respuesta de la api
          this.usuario = new Usuario(response.id, response.username, response.iso_3166_1);
      },
      error => {
        console.log(error);
      }
    );
  }

  //Metodo para cerrar sesion
  logOut(){
    sessionStorage.clear();
    localStorage.clear();
  }

  //Obtener las peliculas favoritas del usuario
  getFavoritas() {
    this._peliculasService.getFavoritas().subscribe(
      response => {
        //Guardamos en una lista la respuesta
        this.listaPeliculas = response.results;
        //Le hacemos un foreach, y creamos con la respuesta un array de peliculas
        this.listaPeliculas.forEach(pelicula => {
          this.peliculas.push(new Pelicula(pelicula.id, pelicula.original_title, 
                                          pelicula.overview, pelicula.release_date, 
                                          "https://image.tmdb.org/t/p/w500/" + pelicula.poster_path,
                                           pelicula.vote_average));
        });
        

        //Este condicional se encarga de mostrar el gif de carga y enviar el mensaje correspondiente
        if(response.results.length == 0) {
          this.message = "Aun no has agregado ninguna pelicula a favoritos."
          this.cargada = true;
        } else {
          this.cargada = true;
        }
      },
      error => {
        console.log(error);
      }
    );
  }

}
